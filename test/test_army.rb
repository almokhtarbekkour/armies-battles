require 'minitest/autorun'
require 'minitest/pride'
require 'minitest/hell'

# require "minitest/reporters"
# Minitest::Reporters.use! Minitest::Reporters::SpecReporter.new

require './src/civilization'
require './src/player'
require './src/battle_history'
require './src/unit'
require './src/pikeman'
require './src/archer'
require './src/knight'
require './src/barrack'
require './src/army'
require './src/battle'

describe Army do
  before do
    @player = Player.new(civilization: "Chinese")
    @player.new_army
    @army = @player.armies.first
  end

  describe 'After create' do
    it "should be an Army" do
      expect(@army.class).must_equal Army
    end

    it 'should has 1.000 gold coins' do
      expect(@army.gold_coins).must_equal 1000
    end

    it 'should has a record of his battles' do
      expect(@army.battle_history.class).must_equal BattleHistory
      expect(@army.battle_history.list_of_battles).must_equal []
    end

    it 'should has a strongest unit' do
      expect(@army.strongest_unit.class).must_equal Knight
    end

    describe 'if his civilization is Chinese ' do
      it 'should have 300 attack points' do
        @chinese_player = Player.new(civilization: "Chinese")
        @chinese_player.new_army
        @chinese_army = @chinese_player.armies.first
        expect(@chinese_army.attack_points).must_equal 300
      end
    end

    describe 'if his civilization is English ' do
      it 'should have 350 attack points' do
        @english_player = Player.new(civilization: "English")
        @english_player.new_army
        @english_army = @english_player.armies.first
        expect(@english_army.attack_points).must_equal 350
      end
    end

    describe 'if his civilization is Byzantine ' do
      it 'should have 405 attack points' do
        @byzantine_player = Player.new(civilization: "Byzantine")
        @byzantine_player.new_army
        @byzantine_army = @byzantine_player.armies.first
        expect(@byzantine_army.attack_points).must_equal 405
      end
    end
  end

  describe 'Train a unit' do
    describe 'When train a pikeman' do
      it 'should gain 3 points' do
        pikeman = @army.units.find { |u| u.class ==  Pikeman }
        point_before = pikeman.strength_points
        @army.train(pikeman)
        improve_by_3_point = point_before + 3

        expect(pikeman.strength_points).must_be :==, improve_by_3_point
      end

      it 'and should cost 10 gold coins' do
        pikeman = @army.units.find { |u| u.class ==  Pikeman }
        gold_coins_before = @army.gold_coins
        @army.train(pikeman)
        coins_after_paying = gold_coins_before - 10

        expect(@army.gold_coins).must_be :==, coins_after_paying
      end
    end
  end

  describe 'Transform a unit' do
    describe 'Conver a Pikeman in to an Archer' do
      it 'should cost 30 gold coins' do
        pikeman = @army.units.find { |u| u.class ==  Pikeman }
        gold_coins_before = @army.gold_coins
        @army.convert(pikeman)
        coins_after_paying = gold_coins_before - 30
        expect(@army.gold_coins).must_be :==, coins_after_paying
      end

      it 'then the Army should gain 5 points' do
        pikeman = @army.units.find { |u| u.class ==  Pikeman }
        attack_points_before = @army.attack_points
        @army.convert(pikeman)
        attack_points_after_transforming = attack_points_before + 5

        expect(@army.attack_points).must_be :==, attack_points_after_transforming
      end
    end

    describe 'When try to conver a Knight' do
      it 'should be denied' do
        knight = @army.units.find { |u| u.class == Knight }
        expect { @army.convert(knight) }.must_raise NonConvertibleUnitError
      end

      it 'shouldn\'t be increased the army attack points' do
        knight = @army.units.find { |u| u.class == Knight }
        points_before = @army.attack_points
        begin
          @army.convert(knight)
        rescue NonConvertibleUnitError => e
        end

        expect(@army.attack_points).must_be :==, points_before
      end
    end
  end

  describe 'Attack another other army' do
    before do
      @player_one = Player.new(civilization: "Byzantine")
      @player_one.new_army
      @player_one_army = @player_one.armies.first

      @player_two = Player.new(civilization: "English")
      @player_two.new_army
      @player_two_army = @player_two.armies.first
    end

    describe "when an army attack another army" do
      it "must start a battle and inform who won" do
        expect( @player_one_army.attack(@player_two_army) ).must_equal :winner
      end

      it 'and after figth an army must record the battle in his own history' do
        expect(@player_one_army.battle_history.list_of_battles).must_equal []
        @player_one_army.attack(@player_two_army)
        expect(@player_one_army.battle_history.list_of_battles).must_be :!=, []
      end
    end
  end
end
