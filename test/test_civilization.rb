require 'minitest/autorun'
require 'minitest/pride'
require 'minitest/hell'
# require "minitest/reporters"
# Minitest::Reporters.use! Minitest::Reporters::SpecReporter.new

require './src/civilization'

describe Civilization do
  before do
    @civilization = Civilization.new
  end

  describe 'after create' do
    it "should be an Civilization type" do
      expect(@civilization.class).must_equal Civilization
    end

    it "should have a name" do
      expect(@civilization.name.empty?).must_be_same_as false
    end
  end

  describe 'define the initial units for his armies ' do
    describe "if is Chinese then initial units" do
      it 'should be { pikeman: 2, archer: 25, knight: 2 }' do
        init_chinese = { pikeman: 2, archer: 25, knight: 2 }
        expect(Civilization.initial_army_units("Chinese")).must_equal init_chinese
      end
    end

    describe "if is English then initial units" do
      it 'should be { pikeman: 10, archer: 10, knight: 10 }' do
        init_english = { pikeman: 10, archer: 10, knight: 10 }
        expect(Civilization.initial_army_units("English")).must_equal init_english
      end
    end

    describe "if is Byzantine then initial units" do
      it 'should be { pikeman: 5, archer: 8, knight: 15 }' do
        init_byzantine = { pikeman: 5, archer: 8, knight: 15 }
        expect(Civilization.initial_army_units("Byzantine")).must_equal init_byzantine
      end
    end
  end
end
