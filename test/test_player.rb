require 'minitest/autorun'
require 'minitest/pride'
require 'minitest/hell'

# require "minitest/reporters"
# Minitest::Reporters.use! Minitest::Reporters::SpecReporter.new

require './src/civilization'
require './src/player'
require './src/battle_history'
require './src/unit'
require './src/pikeman'
require './src/archer'
require './src/knight'
require './src/barrack'
require './src/army'

describe Player do
  before do
    @player = Player.new(name: "Chincu lin", civilization: "Chinese")
  end

  describe 'after create' do
    it "should be an Player" do
      expect(@player.class).must_equal Player
    end

    it "should be an Civilization type" do
      expect(["Chinese", "English", "Byzantine"].include? @player.civilization_name).must_equal true
    end

     it "should have a name" do
      expect(@player.name.empty?).must_be_same_as false
    end

    it "shouldn't have armies" do
      expect(@player.armies.empty?).must_be_same_as true
    end
  end

  describe 'create an army' do
    it "should has an army in @armies" do
      @player.new_army
      @army = @player.armies.first
    end
  end
end


