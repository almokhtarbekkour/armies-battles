
class BattleHistory

  def initialize(battles = [])
    @battles = battles
  end

  def list_of_battles
    @battles
  end

  def add_battle_record(battle)
    @battles << battle
  end
end
