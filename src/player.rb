
class Player
  attr_reader :civilization, :armies, :name

  def initialize(args = {})
    @name = args[:name]
    @civilization = Civilization.new(args[:civilization])
    @armies = []
  end

  def new_army
    @armies << Army.new(self.civilization)
  end

  def civilization_name
    @civilization.name
  end

end
