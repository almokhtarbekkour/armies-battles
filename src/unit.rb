
class Unit
  attr_reader :strength_points, :convertible

  def initialize(args={})
    @strength_points = args[:point]
    @convertible = false
  end

  def add_strength_point(points)
    @strength_points += points
  end
end
