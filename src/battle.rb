
class Battle
  attr_reader :battle_result

  def initialize(args = {})
    @attacking_army = args[:attacking_army]
    @defense_army = args[:defense_army]
    @battle_result = nil
  end

  def start
    set_battle_result

    winner_consequences if @battle_result == :winner
    losser_consequences if @battle_result == :losser
    tied_consequences if @battle_result == :tied

    register_battle_in_armies_history

    return @battle_result
  end

  private

  def set_battle_result
    if @attacking_army.attack_points == @defense_army.attack_points
      @battle_result = :tied
    elsif @attacking_army.attack_points > @defense_army.attack_points
      @battle_result = :winner
    else
      @battle_result = :losser
    end
  end

  def lose_the_the_strongest_unit(army)
    army.destroy_unit army.strongest_unit
  end

  def receive_spoils_of_war(army)
    army.receive_gold(100)
  end

  def winner_consequences
    receive_spoils_of_war(@attacking_army)
    2.times { lose_the_the_strongest_unit( @defense_army) }
  end

  def losser_consequences
    2.times { lose_the_the_strongest_unit( @attacking_army) }
  end

  def tied_consequences
    lose_the_the_strongest_unit( @attacking_army)
    lose_the_the_strongest_unit( @defense_army)
  end

  def register_battle_in_armies_history
    @attacking_army.battle_history.add_battle_record(self)
    @defense_army.battle_history.add_battle_record(self)
  end

end
