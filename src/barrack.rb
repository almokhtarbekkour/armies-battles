class NonConvertibleUnitError < StandardError; end

class Barrack
  TRAIN_COST = {
                pikeman: { cost: 10, gain_strength: 3  },
                archer:  { cost: 20, gain_strength: 7  },
                knight:  { cost: 30, gain_strength: 10 }
              }

  CONVERSION_COST = {
                pikeman: { cost: 30, change_to: :archer, gain_strength: 4 },
                archer:  { cost: 40, change_to: :knight, gain_strength: 3 },
              }

  class << self
    def unit_train_cost(unit)
      TRAIN_COST[unit.class.to_s.downcase.to_sym][:cost]
    end

    def unit_type_gain_strength(unit)
      TRAIN_COST[unit.class.to_s.downcase.to_sym][:gain_strength]
    end

    def unit_convertion_cost(unit)
      CONVERSION_COST[unit.class.to_s.downcase.to_sym][:cost]
    end

    def unit_convertion_to(unit)
      CONVERSION_COST[unit.class.to_s.downcase.to_sym][:change_to]
    end

    def train(unit, army)
      train_cost = unit_train_cost(unit)
      gain_strength = unit_type_gain_strength(unit)

      if army.gold_coins >= train_cost
        unit.add_strength_point(gain_strength)
        army.pay_gold(train_cost)
      end
    end

    def convert(unit, army)
      fail(NonConvertibleUnitError, "Not is possible to convert the unit") unless unit.convertible

      convertion_cost = unit_convertion_cost(unit)
      new_unit_type = unit_convertion_to(unit)

      if army.gold_coins >= convertion_cost && unit.convertible
        army.destroy_unit(unit)
        army.pay_gold(convertion_cost)
        add_unit_type(new_unit_type, army)
      end
    end

    def create_initial_units_army(army)
      Civilization.initial_army_units(army.civilization.name).each do |type, amount|
        amount.times do
          add_unit_type(type, army)
        end
      end
    end

    private

    def add_unit_type(unit_type, army)
      new_unit = self.send("create_#{unit_type}")
      army.add_unit(new_unit)
    end

    def create_pikeman
      Pikeman.new
    end

    def create_archer
      Archer.new
    end

    def create_knight
      Knight.new
    end
  end
end
