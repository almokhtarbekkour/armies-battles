
require_relative 'civilization'
require_relative 'battle_history'
require_relative 'army'
require_relative 'unit'
require_relative 'pikeman'
require_relative 'archer'
require_relative 'knight'
require_relative 'barrack'
require_relative 'player'
require_relative 'battle'

player_one = Player.new(name: "Qin Shi Huang", civilization: "Chinese")
player_one.new_army

puts "Player: #{player_one.civilization_name}"
puts "Armies: #{player_one.armies} \n\r"

player_two = Player.new(name: "Teodosio el Grande", civilization: "Byzantine")
player_two.new_army

puts "Player: #{player_two.civilization_name}"
puts "Armies: #{player_two.armies} \n\r"

puts "Player 1 (#{player_one.civilization_name}) attack the first army of Player 2 (#{player_two.civilization_name}) \n\r"

player_one_army = player_one.armies.first
player_two_army = player_two.armies.first

battle_result = player_one_army.attack(player_two_army)

case battle_result
  when :winner
    puts "Player 1 Win! and take 100 units of gold"
  when :losser
    puts "Player 1 lose the battle!\n He has #{player_one_army.units.count} units."
  else
    puts "Battle tied! all lose."
end
