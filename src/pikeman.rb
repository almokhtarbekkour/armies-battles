
class Pikeman < Unit
  AGREED_POINTS = 5

  def initialize(args = {})
    @convertible = true
    @strength_points = args[:point] || AGREED_POINTS
  end

end
