
class Civilization
  CIVILIZATION_NAMES = %w{ Chinese English Byzantine }
  ARMY_INITIAL_UNITS = {
                    Chinese: { pikeman: 2, archer: 25, knight: 2 },
                    English: { pikeman: 10, archer: 10, knight: 10 },
                    Byzantine: { pikeman: 5, archer: 8, knight: 15 },
                  }

  attr_reader :name

  def initialize(name = CIVILIZATION_NAMES.sample)
    @name = name
  end

  def self.initial_army_units(civilization)
    return nil unless CIVILIZATION_NAMES.include? civilization

    ARMY_INITIAL_UNITS[civilization.to_sym]
  end
end
