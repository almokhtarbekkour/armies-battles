# Armies Battles Challenge - Modeling proposal
This is the Armies Battles challenge modeling proposal for Helix Technologies Inc.
![enter image description here](https://helixtechnologies.com/wp-content/uploads/2019/11/Helix-Technologies-Logo-200x60.png)
See the full text of the challenge at [DESAFIO TECNICO HELIX TCS Version Full.pdf](doc/DESAFIO_TECNICO_HELIX_TCS_Version_Full.pdf)

## Pre requirements
In order to execute the solution you must have install a **Ruby Interpreter** in your system.

Also you must have install minitest gem. For install it run in console:

    gem install minitest

# Proposed model

![Proposed model](doc/modelo_clases_army_battles.jpg)

## Run the solution
Follow the next steps:
 1. Go to **/army_battles** forder.
 2. Open a ruby **console** in that folder /army_battles
 3. In the console, run:
`ruby test/test_battle.rb -v`


## [opcional ] Guard
Youl could use Guard gem to auto-execute the unit test:
1. you must have install Ruby interpreter in your system.
2. run:
`gem install guard`
4. run:
`gem install guard-minitest`
6. run in the console:
`guard`

## [opcional ] Reporter
1. Install the gem minitest-reporters for better test report, run:
`gem install minitest-reporters`
2. Uncomment the following lines in the **test_*.rb** files.
    `# require "minitest/reporters"`
    `# Minitest::Reporters.use! Minitest::Reporters::SpecReporter.new`
